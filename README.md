# FLUTTER TROUBLESHOOTING DOCUMENTATION #

Documentation of problems and solution when creating apps with flutter

---
## Integrating with firebase ###

### Android setup problems

After Flutter Firebase Setup done (see https://fireship.io/snippets/install-flutterfire/) and adding firebase packages to pubspec.yaml file:
	
```yaml
firebase_core: ^0.4.4+3
firebase_auth: ^0.16.0
cloud_firestore: ^0.13.5
```

Error will occur when building the app:

```yaml
Plugin project :firebase_auth_web not found. Please update settings.gradle.

Plugin project :firebase_core_web not found. Please update settings.gradle.

Plugin project :cloud_firestore_web not found. Please update settings.gradle.
```

In order to resolve this, go to: `android/app/build.gradle`

and change min sdk version from 16 to 21
```java
android {
	// ...
	defaultConfig {
		// ...
		minSdkVersion 16
		}
}
	```

Then go to: `main/android/local.properties`

and add at the bottom theese codes:
```java
// ... bottom of file 
def flutterProjectRoot = rootProject.projectDir.parentFile.toPath()

def plugins = new Properties()
def pluginsFile = new File(flutterProjectRoot.toFile(), '.flutter-plugins')
if (pluginsFile.exists()) {
		pluginsFile.withReader('UTF-8') { reader -> plugins.load(reader) }
}

plugins.each { name, path ->
	def pluginDirectory = flutterProjectRoot.resolve(path).resolve('android').toFile()
	include ":$name"
	project(":$name").projectDir = pluginDirectory
}
```

this should resolve the problems and let you build the app just fine.

Source: 
* https://stackoverflow.com/questions/61732409/plugin-project-firebase-core-web-not-found
* https://github.com/FirebaseExtended/flutterfire/issues/2599

---
## Geolocator on emulator can't return promise


When calling the Geolocator function like this:

```dart
import 'package:geolocator/geolocator.dart';

void getLocation() async {
	Position position = await Geolocator().
		getCurrentPosition(desiredAccuracy: LocationAccuracy.low);

	print(position);
}
```

after asking the first permisson and working, It will not escape the promise function after the second or third time it's called.

To resolve this, Get the instance of Geolocator forcing Android Location Manager and after call the getCurrentPosition on this. In this way you can use the LocationManager plugin instead the default FusedLocationProviderClient plugin to retrieve the position.

``` dart
import 'package:geolocator/geolocator.dart';

void getLocation() async {
	Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;
	Position position = await geolocator.
		getCurrentPosition(desiredAccuracy: LocationAccuracy.low);

	print(position);
}
```

The function will work everytime it's called and you can get geo-coordinate just fine.

Source:
  * https://github.com/Baseflow/flutter-geolocator/issues/199#issuecomment-573580819